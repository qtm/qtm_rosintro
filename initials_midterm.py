# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  
        ## This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self, j0, j1, j2, j3, j4, j5):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        ## Planning to a Joint Goal
        # We get the joint values from the group and change some of the values:
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = j0 # this is for the shoulder pan joint
        joint_goal[1] = j1 # this is for the shoulder lift joint
        joint_goal[2] = j2 # this is for the elbow joint
        joint_goal[3] = j3 # this is for the wrist 1 joint
        joint_goal[4] = j4 # this is for the wrist 2 joint
        joint_goal[5] = j5 # this is for the wrist 3 joint

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()
# This function sets the starting point of the robot to avoid any singularities
def start():
    try:
        input(
            "============ Press `Enter` to begin setting up the moveit_commander ..."
        )
        tutorial = MoveGroupPythonInterfaceTutorial()

        input(
            "============ Press `Enter` to set the starting point ..."
        )

        print("Setting the starting point")
        tutorial.go_to_joint_state(0, -tau/8, tau/4, -3*tau/8, -tau/4, 0) # set starting point

        print("============ Start point set")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

# This function writes the initials of my name
def main():
    try:
        input(
            "============ Press `Enter` to set up the moveit_commander again..."
        )
        tutorial = MoveGroupPythonInterfaceTutorial()

        input(
            "============ Press `Enter` to write the initials QTM ..."
        )

        print("Starting point")
        tutorial.go_to_joint_state(0, -tau/8, tau/4, -3*tau/8, -tau/4, 0) # set starting point

        #DRAWING A 'Q'
        print("Starting to draw the Q")
        tutorial.go_to_joint_state(tau, -tau/8, tau/4, -3*tau/8, -tau/4, 0) # rotate 2pi rad to draw the circle of the Q
        tutorial.go_to_joint_state(7*tau/8, -tau/8, tau/4, -3*tau/8, -tau/4, 0) # move pi/4 rad back to set start position of the line in Q
        tutorial.go_to_joint_state(7*tau/8, -1.4*tau/8, 1.4*tau/4, -3.4*tau/8, -tau/4, 0) # move in a line backwards to inside of Q circle
        tutorial.go_to_joint_state(7*tau/8, -0.5*tau/8, 0.5*tau/4, -2.5*tau/8, -tau/4, 0) # move back out to complete the line of the Q outside the circle
        print("Done drawing the Q")

        #TRANSITION BETWEEN 'Q' AND 'T'
        print("Transitioning to the starting point of T")
        tutorial.go_to_joint_state(7*tau/8, -1.4*tau/8, 1.4*tau/4, -3.4*tau/8, -tau/4, 0) # move in a line backwards to inside of Q circle

        #DRAWING A 'T'
        print("Starting to draw the T")
        tutorial.go_to_joint_state(7*tau/8, -tau/8, tau/4, -3*tau/8, -tau/4, 0) # draw vertical line in the T
        tutorial.go_to_joint_state(11*tau/12, -0.95*tau/8, 0.95*tau/4, -2.95*tau/8, -tau/4, 0) # draw left part of horizontal line in the T
        tutorial.go_to_joint_state(7*tau/8, -tau/8, tau/4, -3*tau/8, -tau/4, 0) # move back to the top middle of the T
        tutorial.go_to_joint_state(5*tau/6, -0.95*tau/8, 0.95*tau/4, -2.95*tau/8, -tau/4, 0) # draw right part of horizontal line in the T
        tutorial.go_to_joint_state(7*tau/8, -tau/8, tau/4, -3*tau/8, -tau/4, 0) # move back to the top middle of the T
        print("Done drawing the T")

        #TRANSITION BETWEEN 'T' AND 'M'
        print("Transitioning to the starting point of M")
        tutorial.go_to_joint_state(7*tau/8, -1.4*tau/8, 1.4*tau/4, -3.4*tau/8, -tau/4, 0) # move to starting point for M

        #DRAWING A 'M'
        print("Starting to draw the M")
        tutorial.go_to_joint_state(7*tau/8, -0.85*tau/8, 0.85*tau/4, -2.85*tau/8, -tau/4, 0) # draw first left vertical line in the M
        tutorial.go_to_joint_state(5*tau/6, -1.15*tau/8, 1.15*tau/4, -3.15*tau/8, -tau/4, 0) # draw first diagonal line going right and down in M
        tutorial.go_to_joint_state(19*tau/24, -0.85*tau/8, 0.85*tau/4, -2.85*tau/8, -tau/4, 0) # draw second diagonal line going right and up in M
        tutorial.go_to_joint_state(3*tau/4, -1.4*tau/8, 1.4*tau/4, -3.4*tau/8, -tau/4, 0) # draw last right vertical line in M
        print("Done drawing the M")

        print("============ QTM Drawing Complete!!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return
if __name__ == "__main__":
    start()
    main()