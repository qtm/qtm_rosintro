#!/usr/bin/bash

rosservice call /spawn 7 6.5 0.0 "turtle2"
rosservice call /turtle1/set_pen 252 35 222 4 false
rosservice call /turtle2/set_pen 252 232 10 6 false
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, -4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[1.0, 1.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[-1.0, -1.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[5.0, 7.5, 0.0]' '[0.0, 0.0, 7.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[0.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[1.0, -1.0, 0.0]' '[0.0, 0.0, 1.5]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[-2.3, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /kill "turtle2"
